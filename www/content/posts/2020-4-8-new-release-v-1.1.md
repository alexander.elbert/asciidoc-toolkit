---
title: "New release: v1.1.0 directly available in VI Package Manager"
date: 2020-04-08T16:27:23+01:00
tags: ["release"]
draft: false
---

This version introduces some new features, changes and bug fix you can see below. 

I want to thank people who contribute to this new version with code and ideas:

* [Samuel Taggart](https://gitlab.com/stagg54) who did add unit test to the project
* [Joerg Hampel](https://gitlab.com/joerg.hampel) who did fix bug and had lots of great ideas on the toolkit
* All [LabVIEW User Group members from french Alps](https://forums.ni.com/t5/LUGE-Rh%C3%B4ne-Alpes-et-plus-loin/gp-p/grp-2508) who did a code review of the v1.0 version during our last 2019 session  


but it's also easier to install thanks to the new VIPM Community repository.

![Asciidoc for LabVIEW in VIPM](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/img/asciidoc-toolkit-for-labview-in-vi-package-manager.png)



<!--more-->

### Package Download (if still needed)
[Asciidoc toolkit for LabVIEW v1.1.0](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/vip/wovalab_lib_asciidoc_for_labview-1.1.0.18.vip)

### Installation
Installation can be made with the free version of [VI Package Manager](https://vipm.jki.net/get).

### LabVIEW supported version

2014 (32 and 64 bit) and above

### Release note

#### New features
* Add polymorphic capability to append content (bloc and section) --> [#9](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/9)
* Add function to generate output for sections (skip the document header) --> [#19](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/19)
* Add function to import content from another file into the current document (include directive) --> [#20](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/20)
* Add toc levels attribute --> [#18](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/18)
* Add numbering section attribute --> [#11](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/11)

#### Changes
* Natural section level handling -->  [#12](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/issues/12)
* Section vi icon --> [#10](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/issues/10)
* New Attributes palette functions

#### Bug fix
* Remove useless folder in package --> [#13](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/issues/13)
.[#15](Improve image path handling --> https://gitlab.com/wovalab/open-source/asciidoc-toolkit/issues/15)