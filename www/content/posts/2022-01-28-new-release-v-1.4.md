---
title: "New release: v1.4.0"
date: 2022-01-10T11:23:05+02:00
tags: ["release"]
draft: false
---

This version introduces advanced table formating and fixes some minor bugs:

### Advanced table formating

You can now build modern table including images <1>, admonitions <2> and nested tables <3> in cells.
On top of that, you can also span cells over rows or columns <3>.

![Advanced table built with asciidoc for LabVIEW toolkit](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/img/advanced-table.png)

Building this table is made easy by this new version and you don't need to know anything about asciidoc syntax.

### Contributor

Big thanks to [Paul Morris](https://www.linkedin.com/in/paul-morris-9298012a/) for his contributions to improving the table creation and for the VIs description review.

<!--more-->
### Release note

#### New features:
* Add support for advanced table creation --> [#35](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/35)

#### changes:
* Add description to the public functions --> [#38](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/38)

#### Fix:
* Images dont get rendered if the "alt text" input is an empty string --> [#36](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/36)
* Problem with "Format Text.vi" if the input string begins or ends with a space character. --> [#17](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/17)

### Package Download

The package is directly available through the free version of [VI Package Manager](https://vipm.jki.net/get).

### LabVIEW supported version

2014 (32 and 64 bit) and above