---
title: "First Public Release"
date: 2019-11-03T16:27:23+01:00
tags: ["release"]
draft: false
---

We are happy to release the first public version of the tookit.

![Asciidoc for LabVIEW functions palette](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/img/v1.0.0-function-palette.png)

### Download
[Asciidoc toolkit for LabVIEW v1.0.0](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/vip/wovalab_lib_asciidoc_for_labview-1.0.0.11.vip)

<!--more-->

### Installation
Installation can be made with the free version of [VI Package Manager](https://vipm.jki.net/get).

### LabVIEW supported version

2014 (32 and 64 bit) and above

### Release note

#### Feature
* AscciDoc for LabVIEW palette functions ( located in Addons category)
  * Create Document
  * Add Sections
  * Generate Output
  * Create Sections
  * Add Bloc
  * Create Paragraph
  * Create Table
  * Create Admonition
  * Create Image
  * Asciidoc subpalette
	* Add Line Break
	* Format Text
  
 * Basic example (build a simple file with ascidoc syntax) 
 
#### Bug fixes
 * Let's say that this is quite normal to be empty for v1.0.0 ;)