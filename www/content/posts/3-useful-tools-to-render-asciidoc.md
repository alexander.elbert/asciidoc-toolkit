---
title: "3 Useful Tools to Render Asciidoc"
date: 2020-04-22T07:10:28+02:00
tags: ["tips"]
draft: false
---

Asciidoc syntax is quite readable as a plain text file, but not as easy as the same file rendered as HTML or PDF.

If you want to quickly test the final render of your file the following tools can be useful.


## #1 - Asciidoctor.js Live Preview

Asciidoctor.js Live Preview is a Google Chrome extension that will display any asciidoc file as html directly inside your browser.

![Asciidoctor.js Live Preview in Chrome Toolbar](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/img/asciidoctor.js-live-preview-in-chrome.png)

This is one of the easiest ways to display asciidoc files.

[Download extension here](https://chrome.google.com/webstore/detail/asciidoctorjs-live-previe/iaalpfgpbocpdfblpnhhgllgbdbchmia)

<!--more-->

## #2 - ATOM and asciidoc-preview package

Atom is a text editor that comes with lots of optional packages that add new features. 

One of these packages can add a live preview for your asciidoc files.

After installing asciidoc-preview package, you can display a panel on the right of your text file of the formatted file, that is updated as soon as you modify your text file. This is an easy way to check the format of your asciidoc file.

![Asciidoctor.js Live Preview in Chrome Toolbar](https://wovalab.gitlab.io/open-source/asciidoc-toolkit/img/atom-with-asciidoc-preview.png)

[Download](https://atom.io/users/asciidoctor)

## #3 - AsciiDoctor Docker container 

If you Want to have html or pdf files &nd don’t want to install every asciidoctor tools on your computer and still have HTML and PDF.
Simply use the docker container provided by asciidoc and you will get an up to date installation of the complete tool chain used to render files.

[Download docker container](https://hub.docker.com/r/asciidoctor/docker-asciidoctor)