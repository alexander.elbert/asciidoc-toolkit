---
title: "New release: v1.5.1"
date: 2022-06-27T09:43:05+02:00
tags: ["release"]
draft: false
---

This version mainly fixes and improves the feature added in v1.5.0.

It improves error reporting during the asciidoctor installation document generation.

It also fixes rendering issues.  

## Release note

### Changes:

* LabVIEW error generated on Asciidoctor rendering failure --> [#44](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/44)
* Improve error reporting on toolchain installation --> [#48](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/48)

### Fixes:

* UTF8 support not handle when generating a section --> [#43](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/43)
* Issue in image integration --> [#46](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/46)
* Error on Asciidoctor toolchain destination directory clean up --> [#47](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/47)
* Rendering large diagram fails --> [#51](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/51)

<!--more-->

## Package Download

The package is directly available through the free version of [VI Package Manager](https://vipm.jki.net/get).

## LabVIEW supported version

2014 (32 and 64 bit) and above
